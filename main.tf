terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


provider "aws" {
  region = var.region
}


terraform {
  backend "s3" {
    bucket = "onyxquity-eks-j8e8l9p7"
    key    = "global/s3/terraform.tfstate"
    region = "us-east-1"

    dynamodb_table = "onyxquity-dynamo-eks-j8e8l9p7"
    encrypt        = true
  }
}


locals {
  cluster_name = "onyxquity-eks-${var.cluster_prefix_name}"
}


############################################
#### NETWORK - VPC, Subnets, SGs
#############################################

resource "aws_vpc" "master" {
  cidr_block = var.vpc_cidr

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name                                                = "${local.cluster_name}"
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }
}



resource "aws_subnet" "public" {
  count = length(var.aws_availability_zones[var.region])

  vpc_id            = aws_vpc.master.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index)
  availability_zone = var.aws_availability_zones[var.region][count.index]

  tags = {
    Name                                                = "${local.cluster_name}"
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                            = 1
  }

  map_public_ip_on_launch = true
}


# Internet Gateway
resource "aws_internet_gateway" "public" {
  vpc_id = aws_vpc.master.id

  tags = {
    "Name" = "${local.cluster_name}-igw"
  }

  depends_on = [aws_vpc.master]
}


# Route the public subnet traffic through the IGW
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.master.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.public.id
  }

  tags = {
    Name = "${local.cluster_name}-Default-rt"
  }
}


# Route table and subnet associations
resource "aws_route_table_association" "internet_access" {
  count          = var.availability_zones_count
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.main.id
}


# NAT Elastic IP
resource "aws_eip" "main" {
  vpc = true

  tags = {
    Name = "${local.cluster_name}-ngw-ip"
  }
}


# NAT Gateway
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "${local.cluster_name}-ngw"
  }
}


# Private Subnets
resource "aws_subnet" "private" {
  count = length(var.aws_availability_zones[var.region])

  vpc_id            = aws_vpc.master.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index + length(var.aws_availability_zones[var.region]) + 1)
  availability_zone = var.aws_availability_zones[var.region][count.index]

  tags = {
    Name                                                = "${local.cluster_name}-private-sg"
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"                   = 1
  }
}


# Add route to route table
resource "aws_route" "main" {
  route_table_id         = aws_vpc.master.default_route_table_id
  nat_gateway_id         = aws_nat_gateway.main.id
  destination_cidr_block = "0.0.0.0/0"
}



resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.master.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }

  tags = {
    Name = "private subnet route table"
  }
}



resource "aws_route_table_association" "private" {
  count          = length(var.aws_availability_zones[var.region])
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.private_route_table.id
}




###########################################
### EKS WORKER NODES
###########################################

resource "aws_eks_node_group" "worker" {
  cluster_name    = aws_eks_cluster.control.name
  node_group_name = local.cluster_name
  node_role_arn   = aws_iam_role.node.arn
  subnet_ids      = aws_subnet.private[*].id
  instance_types  = [var.machine_type]

  scaling_config {
    desired_size = var.min_node_count
    max_size     = var.max_node_count
    min_size     = var.min_node_count
  }

  capacity_type = "ON_DEMAND"
  disk_size     = 20

  tags = merge(
    var.tags
  )

  depends_on = [
    aws_iam_role_policy_attachment.node_AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.node_AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.node_AmazonEC2ContainerRegistryReadOnly,
  ]
}



resource "aws_iam_role" "node" {
  name = "${local.cluster_name}-Worker-Role"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "node_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.node.name
}

resource "aws_iam_role_policy_attachment" "node_AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.node.name
}

resource "aws_iam_role_policy_attachment" "node_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.node.name
}



############################################
# EKS Node Security Group
############################################
resource "aws_security_group" "eks_nodes" {
  name        = "${local.cluster_name}-node-sg"
  description = "Security group for all nodes in the cluster"
  vpc_id      = aws_vpc.master.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name                                                = "${local.cluster_name}-node-sg"
    "kubernetes.io/cluster/${local.cluster_name}-cluster" = "owned"
  }
}

resource "aws_security_group_rule" "nodes_internal" {
  description              = "Allow nodes to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.eks_nodes.id
  source_security_group_id = aws_security_group.eks_nodes.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "nodes_cluster_inbound" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_nodes.id
  source_security_group_id = aws_security_group.eks_cluster.id
  to_port                  = 65535
  type                     = "ingress"
}


resource "aws_security_group_rule" "nodes_cluster_outbound" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_nodes.id
  source_security_group_id = aws_security_group.eks_cluster.id
  to_port                  = 65535
  type                     = "egress"
}



################################################
###### EKS Contro Plane
################################################

# EKS Cluster
resource "aws_eks_cluster" "control" {
  name     = "${local.cluster_name}"
  role_arn = aws_iam_role.control_plane.arn

  vpc_config {
    security_group_ids      = [aws_security_group.eks_cluster.id, aws_security_group.eks_nodes.id]
    subnet_ids              = flatten([aws_subnet.public[*].id, aws_subnet.private[*].id])
    endpoint_private_access = true
    endpoint_public_access  = true
    public_access_cidrs     = ["0.0.0.0/0"]
  }

  tags = merge(
    var.tags
  )

  depends_on = [
    aws_iam_role_policy_attachment.cluster_AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.service_AmazonEKSServicePolicy,
  ]
}




# EKS Cluster IAM Role
resource "aws_iam_role" "control_plane" {
  name = "${local.cluster_name}-Cluster-Role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}


resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.control_plane.name
}


resource "aws_iam_role_policy_attachment" "service_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.control_plane.name
}



##############################################
# EKS Cluster Security Group
##############################################
resource "aws_security_group" "eks_cluster" {
  name        = "${local.cluster_name}-cluster-sg"
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.master.id

  tags = {
    Name = "${local.cluster_name}-cluster-sg"
  }
}

resource "aws_security_group_rule" "cluster_inbound" {
  description              = "Allow worker nodes to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_cluster.id
  source_security_group_id = aws_security_group.eks_nodes.id
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster_outbound" {
  description              = "Allow cluster API Server to communicate with the worker nodes"
  from_port                = 1024
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_cluster.id
  source_security_group_id = aws_security_group.eks_nodes.id
  to_port                  = 65535
  type                     = "egress"
}


################################################
##### ECR REPOSITORY
###############################################

resource "aws_ecr_repository" "default" {
  name  = "my-app"
}


################################################
####### Route 53
################################################

data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "www" {
  count   = length(var.aws_elb_hostname)>1 ? 1 : 0
  zone_id = var.aws_zone_id
  name    = var.route53_record_name
  type    = "A"

  alias {
    name                   = var.aws_elb_hostname
    zone_id                = data.aws_elb_hosted_zone_id.main.id
    evaluate_target_health = true
  }
}
